# Repositório de Software

Repositorio destinado para o processamento de dados coletados para permitir o monitoramento do estado do silo a partir da Haste

## Arquitetura

A partir da arquitetura de software definida no [relatório técnico do projeto Siloair](https://relatorio-fga-pi2-semestre-2024-1-grupo-07-e6e361cd1b5ee319a4be.gitlab.io/arquiteturaS04/#arquitetura), foi montado uma esteira em containers do docker para tornar possível esse monitoramento.

## Pré-requisitos para rodar o projeto

### Cenário 1 - Sem processamento de dados

Caso o usuario queira ver apenas a execução do projeto, o computador deverá possuir:

* Conexão com a Internet
* Docker instalado
* 8GB de RAM

### Cenário 2 - Com processamento de dados

Caso o usuario queira testar o consumo de dados com o [subsistema embarcado desenvolvido](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-07/eletronica/controle-de-temperatura/-/tree/main/sistema-de-controle?ref_type=heads) para essa coleta

* Conexão com a Internet
* Docker instalado
* 16GB de RAM

## Execução do projeto

Para construir e rodar o monitoramento de dados, abra o terminal na raiz deste projeto e execute o comando:

```bash
sudo docker compose up -d --build
```

Para desligar o projeto, abra o terminal na raiz deste projeto e execute o comando:
```bash
sudo docker compose down
```

## Fontes 

> [EMQX](https://github.com/emqx/mqtt-to-kafka)
>
> [Minio](https://blog.min.io/kafka_and_minio/)
>
> [Kafka](https://github.com/confluentinc/demo-scene/blob/master/kafka-to-s3/docker-compose.yml)