/**
 * MQTTX Scenario File Example
 * This script generates random temperature and CO2 data.
 */
function generator(faker, options) {
  const sensors = [];
  const total_silos = 1

  // Generate data for temperature sensors
  for (let i = 1; i <= total_silos; i++) {
    // Generate data for 1 CO2 sensor
    sensors.push({
      type: "CO2",
      value: faker.datatype.number({ min: 300, max: 2000 }), // Random CO2 level between 300 and 2000 ppm
      timestamp: faker.date.recent().getTime() / 1000,     // Recent timestamp in seconds
      silo: "silo-" + i                                    // Silo for the Temps sensors
    });
  }

  return {
    // If a topic is not returned, use the topic from the command line arguments.
    message: JSON.stringify(sensors)
  };
}

// Export the scenario module
module.exports = {
  name: 'mock-sensors',  // Scenario name
  generator,      // Generator function
};