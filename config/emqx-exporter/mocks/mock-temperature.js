/**
 * MQTTX Scenario File Example
 * This script generates random temperature and CO2 data.
 */
function generator(faker, options) {
    const sensors = [];
    const total_silos = 1
  
    // Generate data for temperature sensors
    for (let i = 1; i <= total_silos; i++) {
      sensors.push({
        type: "Temperature",
        value: faker.datatype.number({ min: 15, max: 200 }),  // Random temperature between 15 and 35
        timestamp: faker.date.recent().getTime() / 1000,   // Recent timestamp in seconds
        silo: "silo-" + i                                    // Silo for the Temps sensors
      });
    }

    return {
      // If a topic is not returned, use the topic from the command line arguments.
      message: JSON.stringify(sensors)
    };
  }
  
  // Export the scenario module
  module.exports = {
    name: 'mock-sensors',  // Scenario name
    generator,      // Generator function
  };