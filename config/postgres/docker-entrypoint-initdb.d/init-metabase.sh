#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
	CREATE USER ${MB_DB_USER} WITH PASSWORD '${MB_DB_PASS}';
	CREATE DATABASE ${MB_DB_DBNAME} WITH OWNER=${MB_DB_USER};
	COMMIT;
EOSQL

psql -U ${MB_DB_USER} -d ${MB_DB_DBNAME} -f /metabase_default_state.sql