version: "3.8"

x-airflow-common: &airflow-common
  image: ${AIRFLOW_IMAGE_NAME}
  build:
    context: .
    dockerfile: dockerfile.airflow
  environment: &airflow-common-env
    AIRFLOW__CORE__DEFAULT_TIMEZONE: "America/Sao_Paulo"
    AIRFLOW__CORE__ENABLE_XCOM_PICKLING: "true"
    AIRFLOW__CORE__EXECUTOR: LocalExecutor
    AIRFLOW__API__AUTH_BACKENDS: airflow.api.auth.backend.basic_auth
    AIRFLOW__CORE__FERNET_KEY: ${AIRFLOW__CORE__FERNET_KEY}
    AIRFLOW__CORE__LOAD_EXAMPLES: "false"
    AIRFLOW__DATABASE__SQL_ALCHEMY_CONN: postgresql+psycopg2://${POSTGRES_USER:-airflow}:${POSTGRES_PASSWORD:-airflow}@postgres/${POSTGRES_DB:-airflow}
    AIRFLOW__EMAIL__DEFAULT_EMAIL_ON_RETRY: "false"
    AIRFLOW__EMAIL__DEFAULT_EMAIL_ON_FAILURE: "false"
    AIRFLOW__WEBSERVER__DEFAULT_UI_TIMEZONE: "America/Sao_Paulo"
    AIRFLOW__WEBSERVER__INSTANCE_NAME: "Grupo 7 - Local Dev Env!"
    AIRFLOW__WEBSERVER__NAVBAR_COLOR: "#98DFFF"
    AIRFLOW__WEBSERVER__SECRET_KEY: "42"
    AIRFLOW__CORE__TEST_CONNECTION: Enabled
    PYTHONPATH: "${AIRFLOW_HOME}/dags:${AIRFLOW_HOME}/plugins"
  volumes:
    # cluster policy config
    - ./config/airflow/airflow_local_settings.py:${AIRFLOW_HOME}/config/airflow_local_settings.py
    # dags
    - ./dags:${AIRFLOW_HOME}/dags/
    # Plugins
    - ./plugins:${AIRFLOW_HOME}/plugins/plugins

  user: "${AIRFLOW_UID:-50000}:0"
  depends_on: &airflow-common-depends-on
    postgres:
      condition: service_healthy

services:
  postgres:
    build:
      context: ./config/postgres
      dockerfile: ./dockerfile
    env_file:
      - .env
    volumes:
      - ${DB_VOLUME}
    ports:
      - 25570:5432
    healthcheck:
      test: ["CMD", "pg_isready", "-U", "airflow"]
      interval: 10s
      timeout: 120s
      start_period: 180s
      retries: 5
    restart: always

  airflow-webserver:
    <<: *airflow-common
    command: webserver -p 25566
    ports:
      - 25566:25566
    healthcheck:
      test: ["CMD", "curl", "--fail", "http://localhost:25566/health"]
      interval: 10s
      timeout: 180s
      start_period: 180s
      retries: 5
    restart: always
    environment:
      <<: *airflow-common-env
      _AIRFLOW_DB_MIGRATE: "true"
      _AIRFLOW_WWW_USER_CREATE: "true"
      _AIRFLOW_WWW_USER_USERNAME: ${_AIRFLOW_WWW_USER_USERNAME:-airflow}
      _AIRFLOW_WWW_USER_PASSWORD: ${_AIRFLOW_WWW_USER_PASSWORD:-airflow}

  airflow-scheduler:
    <<: *airflow-common
    command: scheduler
    healthcheck:
      test: ["CMD", "curl", "--fail", "http://localhost:8974/health"]
      interval: 30s
      timeout: 10s
      retries: 5
      start_period: 60s
    restart: always
    depends_on:
      <<: *airflow-common-depends-on
      airflow-webserver:
        condition: service_healthy

  metabase:
    image: metabase/metabase:v0.49.7
    ports:
      - 3005:3000
    healthcheck:
      test: curl --fail -I http://localhost:3000/api/health || exit 1
      interval: 15s
      retries: 5
      timeout: 180s
      start_period: 180s
    env_file:
      - .env
    environment:
      MB_DB_TYPE: postgres
      MB_DB_HOST: postgres
      MB_DB_PORT: 5432
    depends_on:
      postgres:
        condition: service_healthy

  EMQX:
    image: emqx/emqx-enterprise:5.0.4
    container_name: emqx
    healthcheck:
      test: ["CMD", "emqx", "ping"]
      interval: 10s
      timeout: 10s
      start_period: 180s
      retries: 12
    ports:
      - 1883:1883
      - 18083:18083
    volumes:
      - ./config/emqx/cluster.hocon:/opt/emqx/data/configs/cluster.hocon
      - ./config/emqx/api_secret.key:/opt/emqx/data/api_secret.key
    environment:
      EMQX_DASHBOARD__BOOTSTRAP_USERS_FILE: '"/opt/emqx/data/api_secret.key"'
    depends_on:
      init-kafka:
        condition: service_completed_successfully

  emqx-exporter:
    depends_on:
      - EMQX
    image: emqx/emqx-exporter
    container_name: exporter-demo
    ports:
      - 8085:8085
    volumes:
      - ./config/emqx-exporter/config/emqx-exporter.config.yaml:/usr/local/emqx-exporter/bin/config.yaml

  prometheus:
    image: prom/prometheus
    container_name: prometheus
    ports:
      - 9090:9090
    volumes:
      - ./config/prometheus/prometheus.yaml:/etc/prometheus/prometheus.yml

  grafana:
    image: grafana/grafana:9.3.2
    container_name: grafana
    ports:
      - 3000:3000
    environment:
      - GF_SECURITY_ADMIN_PASSWORD=public
    volumes:
      - ./config/emqx-exporter/config/grafana-template/EMQX5-enterprise:/grafana-dashboard-emqx5-ee
      - ./config/grafana/provisioning:/etc/grafana/provisioning
      - ./config/grafana/grafana.ini:/etc/grafana/grafana.ini

  minio:
    image: minio/minio
    ports:
      - "9000:9000"
      - "9001:9001"
    volumes:
      - ./mnt/minio:/data
    environment:
      MINIO_ROOT_USER: admin_pi2group7
      MINIO_ROOT_PASSWORD: public_pi2group7
    command: server --console-address ":9001" /data

  kafka:
    image: confluentinc/cp-kafka:7.6.1
    container_name: kafka
    ports:
      - "9092:9092"
      - "19092:19092"
    healthcheck:
      test: kafka-topics --list --bootstrap-server localhost:19092 || exit 1
      interval: 1s
      timeout: 60s
      retries: 60
    environment:
      KAFKA_BROKER_ID: 1
      KAFKA_NODE_ID: 1
      CLUSTER_ID: xPjlEr1nTI6eGXgikjPpKw
      KAFKA_PROCESS_ROLES: broker,controller
      KAFKA_CONTROLLER_LISTENER_NAMES: CONTROLLER
      KAFKA_INTER_BROKER_LISTENER_NAME: IN_DOCKER
      KAFKA_LISTENERS: IN_DOCKER://kafka:9092,OUT_DOCKER://localhost:19092,CONTROLLER://kafka:9093
      KAFKA_ADVERTISED_LISTENERS: IN_DOCKER://kafka:9092,OUT_DOCKER://localhost:19092
      KAFKA_LISTENER_SECURITY_PROTOCOL_MAP: IN_DOCKER:PLAINTEXT,OUT_DOCKER:PLAINTEXT,CONTROLLER:PLAINTEXT
      KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR: 1
      KAFKA_TRANSACTION_STATE_LOG_MIN_ISR: 1
      KAFKA_TRANSACTION_STATE_LOG_REPLICATION_FACTOR: 1
      KAFKA_CONTROLLER_QUORUM_VOTERS: 1@kafka:9093
      KAFKA_AUTO_CREATE_TOPICS_ENABLE: "true"

  init-kafka:
    image: confluentinc/cp-kafka:7.6.1
    entrypoint: [ '/bin/sh', '-c' ]
    command: |
      "
      # blocks until kafka is reachable
      kafka-topics --bootstrap-server kafka:9092 --list

      # Set a retention time of 7 days and a retention size of 1GB for 'silo-sensors'
      echo -e 'Creating kafka topics'
      kafka-topics --bootstrap-server kafka:9092 --create --if-not-exists --topic silo-sensors --replication-factor 1 --partitions 1

      echo -e 'Successfully created the following topics:'
      kafka-topics --bootstrap-server kafka:9092 --list
      "
    depends_on:
      kafka:
        condition: service_healthy

  trino:
    image: trinodb/trino:latest
    container_name: trino
    ports:
      - "25565:8080"
    volumes:
      - ./config/trino/etc:/usr/lib/trino/etc:ro
      - ./config/trino/catalog:/etc/trino/catalog
      - ./config/trino/kafka/silo-sensors.json:/etc/trino/kafka/silo-sensors.json
    depends_on:
      init-kafka:
        condition: service_completed_successfully

  mqttx-stress-test:
    image: emqx/mqttx-cli:v1.9.3
    container_name: mqttx-stress-test
    command: >
      /bin/sh -c "mqttx simulate --file /config/emqx-exporter/mocks/mock.js -c 90 -h EMQX -i 10 -im 10 -t silo-sensors"
    volumes:
      - ./config/emqx-exporter/mocks:/config/emqx-exporter/mocks
    depends_on:
      EMQX:
        condition: service_healthy

