create  TABLE if not exists minio.silo."{TABLE_NAME}" (
    sensor_value DOUBLE,
    creation_datetime timestamp,
    silo VARCHAR,
    sensor_type VARCHAR,
    partition_year integer,
    partition_month integer,
    partition_day integer
)
WITH (
    external_location = 's3a://{BUCKET}/',
    format = 'parquet',
  partitioned_by = ARRAY['silo', 'sensor_type', 'partition_year', 'partition_month', 'partition_day']
)