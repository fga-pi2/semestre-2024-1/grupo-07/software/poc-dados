insert into
	minio.silo."{TABLE_NAME}" (
		sensor_type,
		sensor_value,
		silo,
		creation_datetime,
		partition_year,
		partition_month,
		partition_day
	)
select
	cast(json_extract (element, '$.type') as VARCHAR) as sensor_type,
	cast(json_extract (element, '$.value') as DOUBLE) as sensor_value,
	cast(json_extract (element, '$.silo') as VARCHAR) as silo,
	from_unixtime(floor(to_unixtime("_timestamp"))),
	extract(
		year
		from
			"_timestamp"
	) as ano,
	extract(
		month
		from
			"_timestamp"
	) as mes,
	extract(
		day
		from
			"_timestamp"
	) as ano
from
	kafka."default"."silo-sensors",
	unnest (cast(json_parse (_message) as array < JSON >)) as t (element)
where
	"_timestamp" > (
					select 
	coalesce(max(creation_datetime), (select
							min("_timestamp")
						from
							kafka."default"."silo-sensors"))
				from
			minio.silo."{TABLE_NAME}"
	) and "_timestamp" <= current_timestamp