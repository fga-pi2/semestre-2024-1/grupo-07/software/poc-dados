from airflow.decorators import dag
from airflow.providers.amazon.aws.operators.s3 import S3CreateBucketOperator
from airflow.providers.common.sql.operators.sql import SQLExecuteQueryOperator
from pathlib import Path
from datetime import datetime

MINIO_CONN_ID = "minio_connection"
TRINO_CONN_ID = "trino_connection"

DATA_WAREHOUSE_BUCKET = "data-warehouse"
TABLE_NAME = "silo-sensors"


SCHEMA_DDL_SQL = (
    open(Path(__file__).parent.joinpath("./DDL/create_schema.sql"))
    .read()
    .format(BUCKET=DATA_WAREHOUSE_BUCKET)
)

TABLE_DDL_SQL = (
    open(Path(__file__).parent.joinpath("./DDL/create_table_sensors.sql"))
    .read()
    .format(TABLE_NAME=TABLE_NAME, BUCKET=DATA_WAREHOUSE_BUCKET)
)

INGESTION_SQL = (
    open(Path(__file__).parent.joinpath("./sql/ingestion.sql"))
    .read()
    .format(TABLE_NAME=TABLE_NAME)
)

DEFAULT_ARGS = {"owner": "Paulo G./Laura Pinos"}


@dag(
    default_args=DEFAULT_ARGS,
    tags=["ingestion", "sensor", "IoT"],
    schedule="*/10 * * * *",
    start_date=datetime(2024, 1, 1),
    catchup=False,
)
def ingestion_sensor_data():
    create_bucket_data_warehouse_task = S3CreateBucketOperator(
        task_id="create_bucket_data_warehouse",
        bucket_name=DATA_WAREHOUSE_BUCKET,
        aws_conn_id=MINIO_CONN_ID,
    )

    create_schema_task = SQLExecuteQueryOperator(
        task_id="create_sensors_schema",
        sql=SCHEMA_DDL_SQL,
        conn_id=TRINO_CONN_ID,
    )

    create_table_task = SQLExecuteQueryOperator(
        task_id="create_table_schema",
        sql=TABLE_DDL_SQL,
        conn_id=TRINO_CONN_ID,
    )

    ingest_data = SQLExecuteQueryOperator(
        task_id="ingest_data",
        sql=INGESTION_SQL,
        conn_id=TRINO_CONN_ID,
    )

    create_bucket_data_warehouse_task >> create_schema_task >> create_table_task >> ingest_data


ingestion_sensor_data()
